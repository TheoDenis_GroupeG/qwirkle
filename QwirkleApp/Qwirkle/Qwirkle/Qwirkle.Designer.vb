﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Qwirkle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Qwirkle))
        Me.Counter_Turn = New System.Windows.Forms.Label()
        Me.P_Turn = New System.Windows.Forms.Label()
        Me.Turn_End_Button = New System.Windows.Forms.Button()
        Me.Game_Quit_Button = New System.Windows.Forms.Button()
        Me.P1_stats = New System.Windows.Forms.GroupBox()
        Me.P1_score_last = New System.Windows.Forms.Label()
        Me.P1_Score = New System.Windows.Forms.Label()
        Me.P2_stats = New System.Windows.Forms.GroupBox()
        Me.P2_score_last = New System.Windows.Forms.Label()
        Me.P2_score = New System.Windows.Forms.Label()
        Me.P3_stats = New System.Windows.Forms.GroupBox()
        Me.P3_score_last = New System.Windows.Forms.Label()
        Me.P3_score = New System.Windows.Forms.Label()
        Me.P4_stats = New System.Windows.Forms.GroupBox()
        Me.P4_score_last = New System.Windows.Forms.Label()
        Me.P4_score = New System.Windows.Forms.Label()
        Me.P_Hand = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Reroll = New System.Windows.Forms.GroupBox()
        Me.Reroll_space = New System.Windows.Forms.TableLayoutPanel()
        Me.Reroll_Button = New System.Windows.Forms.Button()
        Me.Options = New System.Windows.Forms.Button()
        Me.Scoreboard = New System.Windows.Forms.GroupBox()
        Me.Board = New System.Windows.Forms.TableLayoutPanel()
        Me.P1_stats.SuspendLayout()
        Me.P2_stats.SuspendLayout()
        Me.P3_stats.SuspendLayout()
        Me.P4_stats.SuspendLayout()
        Me.P_Hand.SuspendLayout()
        Me.Reroll.SuspendLayout()
        Me.Scoreboard.SuspendLayout()
        Me.SuspendLayout()
        '
        'Counter_Turn
        '
        Me.Counter_Turn.AutoSize = True
        Me.Counter_Turn.Location = New System.Drawing.Point(810, 10)
        Me.Counter_Turn.Name = "Counter_Turn"
        Me.Counter_Turn.Size = New System.Drawing.Size(45, 17)
        Me.Counter_Turn.TabIndex = 2
        Me.Counter_Turn.Text = "tour 0"
        '
        'P_Turn
        '
        Me.P_Turn.AutoSize = True
        Me.P_Turn.Location = New System.Drawing.Point(170, 10)
        Me.P_Turn.Name = "P_Turn"
        Me.P_Turn.Size = New System.Drawing.Size(62, 17)
        Me.P_Turn.TabIndex = 3
        Me.P_Turn.Text = "Tour de "
        '
        'Turn_End_Button
        '
        Me.Turn_End_Button.Location = New System.Drawing.Point(763, 431)
        Me.Turn_End_Button.Name = "Turn_End_Button"
        Me.Turn_End_Button.Size = New System.Drawing.Size(100, 30)
        Me.Turn_End_Button.TabIndex = 4
        Me.Turn_End_Button.Text = "Fin du Tour"
        Me.Turn_End_Button.UseVisualStyleBackColor = True
        '
        'Game_Quit_Button
        '
        Me.Game_Quit_Button.Location = New System.Drawing.Point(560, 431)
        Me.Game_Quit_Button.Name = "Game_Quit_Button"
        Me.Game_Quit_Button.Size = New System.Drawing.Size(75, 30)
        Me.Game_Quit_Button.TabIndex = 5
        Me.Game_Quit_Button.Text = "Abandonner"
        Me.Game_Quit_Button.UseVisualStyleBackColor = True
        '
        'P1_stats
        '
        Me.P1_stats.BackColor = System.Drawing.SystemColors.Control
        Me.P1_stats.Controls.Add(Me.P1_score_last)
        Me.P1_stats.Controls.Add(Me.P1_Score)
        Me.P1_stats.Location = New System.Drawing.Point(5, 20)
        Me.P1_stats.Name = "P1_stats"
        Me.P1_stats.Size = New System.Drawing.Size(130, 60)
        Me.P1_stats.TabIndex = 7
        Me.P1_stats.TabStop = False
        Me.P1_stats.Text = "Joueur 1"
        '
        'P1_score_last
        '
        Me.P1_score_last.AutoSize = True
        Me.P1_score_last.Location = New System.Drawing.Point(79, 29)
        Me.P1_score_last.Name = "P1_score_last"
        Me.P1_score_last.Size = New System.Drawing.Size(34, 17)
        Me.P1_score_last.TabIndex = 1
        Me.P1_score_last.Text = "(+0)"
        '
        'P1_Score
        '
        Me.P1_Score.AutoSize = True
        Me.P1_Score.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P1_Score.Location = New System.Drawing.Point(6, 19)
        Me.P1_Score.Name = "P1_Score"
        Me.P1_Score.Size = New System.Drawing.Size(26, 29)
        Me.P1_Score.TabIndex = 0
        Me.P1_Score.Text = "0"
        '
        'P2_stats
        '
        Me.P2_stats.Controls.Add(Me.P2_score_last)
        Me.P2_stats.Controls.Add(Me.P2_score)
        Me.P2_stats.Location = New System.Drawing.Point(5, 110)
        Me.P2_stats.Name = "P2_stats"
        Me.P2_stats.Size = New System.Drawing.Size(130, 60)
        Me.P2_stats.TabIndex = 8
        Me.P2_stats.TabStop = False
        Me.P2_stats.Text = "Joueur 2"
        '
        'P2_score_last
        '
        Me.P2_score_last.AutoSize = True
        Me.P2_score_last.Location = New System.Drawing.Point(79, 29)
        Me.P2_score_last.Name = "P2_score_last"
        Me.P2_score_last.Size = New System.Drawing.Size(34, 17)
        Me.P2_score_last.TabIndex = 1
        Me.P2_score_last.Text = "(+0)"
        '
        'P2_score
        '
        Me.P2_score.AutoSize = True
        Me.P2_score.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P2_score.Location = New System.Drawing.Point(6, 19)
        Me.P2_score.Name = "P2_score"
        Me.P2_score.Size = New System.Drawing.Size(26, 29)
        Me.P2_score.TabIndex = 0
        Me.P2_score.Text = "0"
        '
        'P3_stats
        '
        Me.P3_stats.Controls.Add(Me.P3_score_last)
        Me.P3_stats.Controls.Add(Me.P3_score)
        Me.P3_stats.Location = New System.Drawing.Point(5, 200)
        Me.P3_stats.Name = "P3_stats"
        Me.P3_stats.Size = New System.Drawing.Size(130, 60)
        Me.P3_stats.TabIndex = 8
        Me.P3_stats.TabStop = False
        Me.P3_stats.Text = "Joueur 3"
        '
        'P3_score_last
        '
        Me.P3_score_last.AutoSize = True
        Me.P3_score_last.Location = New System.Drawing.Point(79, 29)
        Me.P3_score_last.Name = "P3_score_last"
        Me.P3_score_last.Size = New System.Drawing.Size(34, 17)
        Me.P3_score_last.TabIndex = 1
        Me.P3_score_last.Text = "(+0)"
        '
        'P3_score
        '
        Me.P3_score.AutoSize = True
        Me.P3_score.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P3_score.Location = New System.Drawing.Point(6, 19)
        Me.P3_score.Name = "P3_score"
        Me.P3_score.Size = New System.Drawing.Size(26, 29)
        Me.P3_score.TabIndex = 0
        Me.P3_score.Text = "0"
        '
        'P4_stats
        '
        Me.P4_stats.Controls.Add(Me.P4_score_last)
        Me.P4_stats.Controls.Add(Me.P4_score)
        Me.P4_stats.Location = New System.Drawing.Point(5, 290)
        Me.P4_stats.Name = "P4_stats"
        Me.P4_stats.Size = New System.Drawing.Size(130, 60)
        Me.P4_stats.TabIndex = 8
        Me.P4_stats.TabStop = False
        Me.P4_stats.Text = "Joueur 4"
        '
        'P4_score_last
        '
        Me.P4_score_last.AutoSize = True
        Me.P4_score_last.Location = New System.Drawing.Point(79, 29)
        Me.P4_score_last.Name = "P4_score_last"
        Me.P4_score_last.Size = New System.Drawing.Size(34, 17)
        Me.P4_score_last.TabIndex = 1
        Me.P4_score_last.Text = "(+0)"
        '
        'P4_score
        '
        Me.P4_score.AutoSize = True
        Me.P4_score.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.P4_score.Location = New System.Drawing.Point(6, 19)
        Me.P4_score.Name = "P4_score"
        Me.P4_score.Size = New System.Drawing.Size(26, 29)
        Me.P4_score.TabIndex = 0
        Me.P4_score.Text = "0"
        '
        'P_Hand
        '
        Me.P_Hand.Controls.Add(Me.TableLayoutPanel1)
        Me.P_Hand.Location = New System.Drawing.Point(10, 411)
        Me.P_Hand.Name = "P_Hand"
        Me.P_Hand.Size = New System.Drawing.Size(220, 61)
        Me.P_Hand.TabIndex = 8
        Me.P_Hand.TabStop = False
        Me.P_Hand.Text = "Vous"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(11, 22)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(196, 28)
        Me.TableLayoutPanel1.TabIndex = 9
        '
        'Reroll
        '
        Me.Reroll.Controls.Add(Me.Reroll_space)
        Me.Reroll.Location = New System.Drawing.Point(236, 411)
        Me.Reroll.Name = "Reroll"
        Me.Reroll.Size = New System.Drawing.Size(220, 60)
        Me.Reroll.TabIndex = 10
        Me.Reroll.TabStop = False
        Me.Reroll.Text = "Remplacer"
        '
        'Reroll_space
        '
        Me.Reroll_space.ColumnCount = 7
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.Location = New System.Drawing.Point(11, 22)
        Me.Reroll_space.Name = "Reroll_space"
        Me.Reroll_space.RowCount = 1
        Me.Reroll_space.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Reroll_space.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.Reroll_space.Size = New System.Drawing.Size(196, 28)
        Me.Reroll_space.TabIndex = 9
        '
        'Reroll_Button
        '
        Me.Reroll_Button.Location = New System.Drawing.Point(462, 431)
        Me.Reroll_Button.Name = "Reroll_Button"
        Me.Reroll_Button.Size = New System.Drawing.Size(60, 30)
        Me.Reroll_Button.TabIndex = 11
        Me.Reroll_Button.Text = "Reroll"
        Me.Reroll_Button.UseVisualStyleBackColor = True
        '
        'Options
        '
        Me.Options.BackgroundImage = CType(resources.GetObject("Options.BackgroundImage"), System.Drawing.Image)
        Me.Options.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Options.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Options.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Options.FlatAppearance.BorderSize = 0
        Me.Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Options.Location = New System.Drawing.Point(10, 5)
        Me.Options.Margin = New System.Windows.Forms.Padding(0)
        Me.Options.Name = "Options"
        Me.Options.Size = New System.Drawing.Size(24, 24)
        Me.Options.TabIndex = 12
        Me.Options.UseMnemonic = False
        Me.Options.UseVisualStyleBackColor = False
        Me.Options.UseWaitCursor = True
        '
        'Scoreboard
        '
        Me.Scoreboard.Controls.Add(Me.P4_stats)
        Me.Scoreboard.Controls.Add(Me.P3_stats)
        Me.Scoreboard.Controls.Add(Me.P2_stats)
        Me.Scoreboard.Controls.Add(Me.P1_stats)
        Me.Scoreboard.Location = New System.Drawing.Point(10, 35)
        Me.Scoreboard.Name = "Scoreboard"
        Me.Scoreboard.Size = New System.Drawing.Size(140, 360)
        Me.Scoreboard.TabIndex = 8
        Me.Scoreboard.TabStop = False
        Me.Scoreboard.Text = "Classement"
        '
        'Board
        '
        Me.Board.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Board.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Board.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.Board.ColumnCount = 11
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.090909!))
        Me.Board.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Board.Location = New System.Drawing.Point(173, 35)
        Me.Board.Margin = New System.Windows.Forms.Padding(0)
        Me.Board.Name = "Board"
        Me.Board.RowCount = 11
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090908!))
        Me.Board.Size = New System.Drawing.Size(690, 360)
        Me.Board.TabIndex = 0
        '
        'Qwirkle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 487)
        Me.Controls.Add(Me.Scoreboard)
        Me.Controls.Add(Me.Options)
        Me.Controls.Add(Me.Reroll_Button)
        Me.Controls.Add(Me.Reroll)
        Me.Controls.Add(Me.P_Hand)
        Me.Controls.Add(Me.Game_Quit_Button)
        Me.Controls.Add(Me.Turn_End_Button)
        Me.Controls.Add(Me.P_Turn)
        Me.Controls.Add(Me.Counter_Turn)
        Me.Controls.Add(Me.Board)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Qwirkle"
        Me.Text = "Qwirkle"
        Me.P1_stats.ResumeLayout(False)
        Me.P1_stats.PerformLayout()
        Me.P2_stats.ResumeLayout(False)
        Me.P2_stats.PerformLayout()
        Me.P3_stats.ResumeLayout(False)
        Me.P3_stats.PerformLayout()
        Me.P4_stats.ResumeLayout(False)
        Me.P4_stats.PerformLayout()
        Me.P_Hand.ResumeLayout(False)
        Me.Reroll.ResumeLayout(False)
        Me.Scoreboard.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Counter_Turn As Label
    Friend WithEvents P_Turn As Label
    Friend WithEvents Turn_End_Button As Button
    Friend WithEvents Game_Quit_Button As Button
    Friend WithEvents P1_stats As GroupBox
    Friend WithEvents P1_score_last As Label
    Friend WithEvents P1_Score As Label
    Friend WithEvents P2_stats As GroupBox
    Friend WithEvents P2_score_last As Label
    Friend WithEvents P2_score As Label
    Friend WithEvents P3_stats As GroupBox
    Friend WithEvents P3_score_last As Label
    Friend WithEvents P3_score As Label
    Friend WithEvents P4_stats As GroupBox
    Friend WithEvents P4_score_last As Label
    Friend WithEvents P4_score As Label
    Friend WithEvents P_Hand As GroupBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Reroll As GroupBox
    Friend WithEvents Reroll_space As TableLayoutPanel
    Friend WithEvents Reroll_Button As Button
    Friend WithEvents Options As Button
    Friend WithEvents Scoreboard As GroupBox
    Friend WithEvents Board As TableLayoutPanel
End Class
