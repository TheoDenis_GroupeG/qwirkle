﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Projet_qwirkle
{
    public class pion
    {
        private int forme=0;
        private int couleur=0;
        private bool verouiller = false;

        public int get_forme() { return this.forme; }
        public int get_couleur() { return this.couleur; }
        public void set_forme(int forme) { this.forme = forme; }
        public void set_couleur(int couleur) { this.couleur = couleur; }
        public void locking() { this.verouiller = true; }
        public void pion_vide() { this.forme = 0;this.couleur=0; }
        

        public string Get_forme()
        {
            
            switch (this.forme)
            {
                case 1:
                    return "■";
                case 2:
                    return  "♥";
                case 3:
                    return "♦";
                case 4:
                    return "♣";
                case 5:
                    return "♠";
                case 6:
                    return "★ ";
                case 7:
                    return "▲";
                case 8:
                    return "∞";
                case 9:
                    return "•";
            }
                    return " ";
        }

        public Color Get_couleur()
        {
            
            switch(this.couleur)
            {
                case 1:
                    return Color.Red;
                case 2:
                    return Color.Blue;
                case 3:
                    return Color.Green;
                case 4:
                    return Color.Brown;
                case 5:
                    return Color.Yellow;
                case 6:
                    return Color.Orange;
                case 7:
                    return Color.Pink;
                case 8:
                    return Color.Purple;
                case 9:
                    return Color.Indigo;

            }
            return Color.White;
        }
    }
}
