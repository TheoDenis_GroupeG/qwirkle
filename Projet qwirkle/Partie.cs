﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_qwirkle
{
    public class Partie
    {
        private int nbr_joueur;
        private int nbr_tour;
        private int id_partie;
        private int tour_joueur;
        private int[,] dificulté = new int[10,10];
        private main[] player;
        public Pioche pioche;

        public Partie(int nbr_joueur,int[,]dificulté)
        {
            //initialisation a la fois des instance et de la classe
            this.nbr_joueur = nbr_joueur;
            this.dificulté = dificulté;
            player = new main[nbr_joueur];
            pioche = new Pioche(dificulté);
        }
        
        public void distribution(int nbr_joueur)
        {
            //remplie les main des joueur

        }
        public int get_tour() { return this.nbr_tour; }
        public int get_id() { return this.id_partie; }
        public int get_qui_joue() { return this.tour_joueur; }
        public int get_nbr_joueur() { return this.nbr_joueur; }
        public main[] fin_partie()
        {
            main[] retour = new main[nbr_joueur];
            main swap1 = player[0];
            main[] swap2 = this.player;
            int position=0;
            //fonction qui creer un ordre de point 
            for (int iteration2=0;iteration2<nbr_joueur-1;iteration2++)
            {
                for (int iteration = iteration2; iteration <= nbr_joueur - 1; iteration++)
                {
                    if (swap1.get_score() < swap2[iteration].get_score())
                    {
                        swap1 = swap2[iteration];
                        position = iteration;
                    }
                }
                retour[iteration2] = swap2[position];
            }
            return retour;
        }
    }
}
