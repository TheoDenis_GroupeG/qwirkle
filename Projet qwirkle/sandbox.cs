﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_qwirkle
{
    class sandbox
    {
        private pion[,] plateau = new pion[30, 30];
        private pion[,] plateau_swap = new pion[30, 30];
        private int[] dimension = new int[2] { 11, 11 };

        public pion[,] get_plateau() { return this.plateau; }
        public pion[,] get_plateau_swap() { return this.plateau_swap; }

        public bool patrick(int coox,int cooy,pion objet)
        { 
        int x1 = 0, x2 = 0, y1 = 0, y2 = 0;//pourquoi x et y ? x représente l'axe x et et y l'axe y
            if (x1 == 111 || x2 == 111 || y1 == 111 || y2 == 111) { return false; }

            x1 = test_haut(coox, cooy, objet);
            x2 = test_bas(coox, cooy, objet);
            y1 = test_gauche(coox, cooy, objet);
            y2 = test_droite(coox, cooy, objet);

            if (x1==-1||x2==-1||y1==-1||y2==-1) { return false; }
            if ((y1 >= 1 && y2 >= 1))
            {
                test_horizontal(coox, cooy, objet);
            }
            if ((x1 >= 1 && x2 >= 1))
            {
                test_vertical(coox,cooy,objet);
            }

            if ((x2 == 0 && x1 == 0 && y2 == 0 && y1 == 0)) { return false; }
            plateau_swap[coox, cooy] = objet;
            return true;
        }
        public int test_haut(int coox,int cooy, pion objet)
        {int x = 0;
            if (plateau_swap[coox + 1, cooy].get_couleur() != 0){
                x = 1;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur()) {
                    x += 10;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0) ); iteration++){
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox + iteration, cooy].get_couleur()){
                            return -1;
                        }
                    }
                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox + 1, cooy].get_forme()){
                    x += 100;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0) ); iteration++){
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox + iteration, cooy].get_forme()){
                            return -1;
                        }
                    }
                }
            }
            return x;
        }

        public int test_bas(int coox, int cooy, pion objet)
        {int x;
            {x = 1;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur()){
                    x += 10;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0) ); iteration++){
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox - iteration, cooy].get_couleur()){
                            return -1;
                        }
                    }
                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox - 1, cooy].get_forme()){
                    x += 100;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0) ); iteration++){
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox - iteration, cooy].get_forme()){
                            return -1;
                        }
                    }
                }
            }
            return x;}

        public int test_gauche(int coox, int cooy, pion objet)
        {int y;
             y = 1; 
             if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur()){
                  y += 10;
                    for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_couleur() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0)); iteration++){
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy - iteration].get_couleur()){
                            return -1;
                        }
                    }
                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy - 1].get_forme()){
                    y += 100;
                    for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_forme() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0) ); iteration++){
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy - iteration].get_forme()){
                            return +1;
                        }
                    }
                }
                return y;}

        public int test_droite(int coox,int cooy,pion objet)
        {int y =0;

            {
                y = 1;
                
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur())
                {
                    y += 10;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0)); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy + iteration].get_couleur())
                        {
                            return -1;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy + 1].get_forme())
                {
                    y += 100;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0) ); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy + iteration].get_forme())
                        {
                            return -1;
                        }
                    }
                }
                return y;
            }
        }

        public bool test_vertical(int coox,int cooy,pion objet)
        {
            
            for (int iteration = 1; plateau_swap[coox - iteration, cooy].get_couleur() == 0; iteration++)
            {
                for (int iteration2 = 1; plateau_swap[coox + iteration2, cooy].get_couleur() == 0; iteration2++)
                {
                    if ((plateau_swap[coox + iteration2, cooy].get_couleur() == plateau_swap[coox - iteration, cooy].get_couleur()) && (plateau_swap[coox + iteration2, cooy].get_forme() == plateau_swap[coox - iteration, cooy].get_forme()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool test_horizontal(int coox,int cooy, pion objet)
        {
            for (int iteration = 1; plateau_swap[coox, cooy - iteration].get_couleur() == 0; iteration++)
            {
                for (int iteration2 = 1; plateau_swap[coox, cooy + iteration2].get_couleur() == 0; iteration2++)
                {
                    if ((plateau_swap[coox, cooy + iteration2].get_couleur() == plateau_swap[coox, cooy - iteration].get_couleur()) && (plateau_swap[coox, cooy + iteration2].get_forme() == plateau_swap[coox, cooy - iteration].get_forme()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool test_placement(int coox, int cooy, pion objet)
        {
            bool impossible = false;
            int x1 = 0, x2 = 0, y1 = 0, y2 = 0;//pourquoi x et y ? x représente l'axe x et et y l'axe y
            int arreteur = 0;
            if (plateau_swap[coox - 1, cooy].get_couleur() != 0)
            {
                x2 = 1;
                arreteur = 0;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur())
                {
                    x2 += 10;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox - iteration, cooy].get_couleur())
                        {
                            return false;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox - 1, cooy].get_forme())
                {
                    x2 += 100;
                    for (int iteration = 2; ((plateau_swap[coox - iteration, cooy].get_couleur() != 0 && plateau_swap[coox - iteration, cooy].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox - iteration, cooy].get_forme())
                        {
                            return false;
                        }
                    }
                }
            }
            if (plateau_swap[coox + 1, cooy].get_couleur() != 0)
            {
                x1 = 1;
                arreteur = 0;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox - 1, cooy].get_couleur())
                {
                    x1 += 10;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox + iteration, cooy].get_couleur())
                        {
                            return false;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox + 1, cooy].get_forme())
                {
                    x1 += 100;
                    for (int iteration = 2; ((plateau_swap[coox + iteration, cooy].get_couleur() != 0 && plateau_swap[coox + iteration, cooy].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox + iteration, cooy].get_forme())
                        {
                            return false;
                        }
                    }
                }

            }
            if (plateau_swap[coox, cooy - 1].get_couleur() != 0)
            {
                y1 = 1;
                arreteur = 0;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur())
                {
                    y1 += 10;
                    for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_couleur() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy - iteration].get_couleur())
                        {
                            return false;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy - 1].get_forme())
                {
                    y1 += 100;
                    for (int iteration = 2; ((plateau_swap[coox, cooy - iteration].get_couleur() != 0 && plateau_swap[coox, cooy - iteration].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy - iteration].get_forme())
                        {
                            return false;
                        }
                    }
                }
            }
            if (plateau_swap[coox, cooy + 1].get_couleur() != 0)
            {
                y2 = 1;
                arreteur = 0;
                if (plateau_swap[coox, cooy].get_couleur() == plateau_swap[coox, cooy - 1].get_couleur())
                {
                    y2 += 10;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_couleur() != plateau_swap[coox, cooy + iteration].get_couleur())
                        {
                            return false;
                        }
                    }

                }
                if (plateau_swap[coox, cooy].get_forme() == plateau_swap[coox, cooy + 1].get_forme())
                {
                    y2 += 100
;
                    for (int iteration = 2; ((plateau_swap[coox, cooy + iteration].get_couleur() != 0 && plateau_swap[coox, cooy + iteration].get_forme() != 0) && arreteur == 1); iteration++)
                    {
                        if (plateau_swap[coox, cooy].get_forme() != plateau_swap[coox, cooy + iteration].get_forme())
                        {
                            return false;
                        }
                    }
                }
            }

            if (x1 == 111 || x2 == 111 || y1 == 111 || y2 == 111) { return false; }

            if ((x1 == 1 && x2 == 1))
            {
                for (int iteration = 1; plateau_swap[coox - iteration, cooy].get_couleur() == 0; iteration++)
                {
                    for (int iteration2 = 1; plateau_swap[coox + iteration2, cooy].get_couleur() == 0; iteration2++)
                    {
                        if ((plateau_swap[coox + iteration2, cooy].get_couleur() == plateau_swap[coox - iteration, cooy].get_couleur()) && (plateau_swap[coox + iteration2, cooy].get_forme() == plateau_swap[coox - iteration, cooy].get_forme()))
                        {
                            return false;
                        }
                    }
                }
            }
            if ((y1 == 1 && y2 == 1))
            {
                for (int iteration = 1; plateau_swap[coox, cooy - iteration].get_couleur() == 0; iteration++)
                {
                    for (int iteration2 = 1; plateau_swap[coox, cooy + iteration2].get_couleur() == 0; iteration2++)
                    {
                        if ((plateau_swap[coox, cooy + iteration2].get_couleur() == plateau_swap[coox, cooy - iteration].get_couleur()) && (plateau_swap[coox, cooy + iteration2].get_forme() == plateau_swap[coox, cooy - iteration].get_forme()))
                        {
                            return false;
                        }
                    }
                }
            }






            if ((x2 == 0 && x1 == 0 && y2 == 0 && y1 == 0) || impossible == true) { return false; }
            plateau_swap[coox, cooy] = objet;
            return true;
        }
    }

}
