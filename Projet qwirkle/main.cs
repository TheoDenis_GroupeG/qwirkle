﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet_qwirkle
{
    public class main
    {
        private pion[] Main = new pion[7];
        private int score = 0;
        private joueur player;
        public main()
        {
            player = new joueur();
        }
            
        public pion[] get_main(){return this.Main;}
        public int get_score() { return this.score;}
        public void ajout_score(int points) { this.score += points; }
        public void set_main(pion[] Main) { this.Main = Main; }
    }
}
