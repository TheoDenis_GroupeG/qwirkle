﻿using System;

namespace Projet_qwirkle
{
    public class Pioche
    {
        private int nbr_pions;
        private int[,] pions_dispo= new int[10,10];

        public Pioche(int[,] difficulté)
        {
            for (int iteration=0;iteration<=9;iteration++)
            {
                pions_dispo[iteration,0] = difficulté[iteration,0];
            }
            for (int iteration = 0; iteration <= 9; iteration++)
            {
                pions_dispo[iteration, 1] = difficulté[0,iteration];
            }
            for (int iterationx =1;iterationx<=9;iterationx++)
            {
                if (pions_dispo[iterationx, 0] == 1)
                {
                    for (int iterationy = 1; iterationy <= 9; iterationy++)
                    {
                        if (pions_dispo[0, iterationy] == 1)
                        {
                            pions_dispo[iterationx, iterationy] = 3;
                        }
                    }   
                }   
            }
        }
        public int get_nbr_pions() { return this.nbr_pions; }
        public int[,] get_pions_dispo() { return this.pions_dispo; }
        public void retour_pions(pion objet)
        {
            pions_dispo[objet.get_couleur(), objet.get_forme()] += 1;
        }
    }
}
