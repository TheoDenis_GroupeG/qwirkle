﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Projet_qwirkle
{
    public class birthdate
    {
        int day;
        int month;
        int year;
        DateTime today = DateTime.Today;
        public birthdate(int day, int month, int year)
        {
            this.day = day; this.month = month; this.year = year;
        }
        public int Day { get => day; }
        public int Month { get => month; }
        public int Year { get => year; }
        public int age()
        {
            int year;

            year = today.Year - this.year ;
            return year;
        }


    }
    public class joueur
    {
        private string pseudo;
        public birthdate age;
        //public img photo 

        public joueur(string pseudo, int day,int month,int year)
        {
            this.pseudo = pseudo;
            age = new birthdate(day, month, year);
        }
        public string get_pseudo() { return this.pseudo; }
        public int get_age(){return age.age();}
        public void set_pseudo(string pseudo) { this.pseudo = pseudo; }
        public void set_age(int day,int month,int year ) {this.age = new birthdate(day, month, year);}
        /*
        public void set_photo(img photo){this.photo=photo;}
        public img get_photo(){return this.photo;}
         */
    }
}
